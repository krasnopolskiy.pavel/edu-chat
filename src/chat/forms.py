from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.validators import UnicodeUsernameValidator

from chat.models import Chat, Message, ChatMember


class RegisterForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'password1']


class LoginForm(AuthenticationForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'password']


class ConnectForm(forms.Form):
    chat_id = forms.CharField(label='Room unique ID', max_length=64)
    # password = forms.CharField(label='Password',  widget=forms.PasswordInput())


class CreateForm(forms.ModelForm):
    class Meta:
        model = Chat
        fields = ['name']


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['text']
        widgets = {
            'text': forms.Textarea()
        }
        labels = {'text': ''}


class ChatMemberForm(forms.Form):
    user = forms.CharField(
        help_text="150 characters or fewer. Letters, digits and @/./+/-/_ only.",
        validators=[UnicodeUsernameValidator()]
    )

