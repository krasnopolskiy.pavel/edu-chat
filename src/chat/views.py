from django.shortcuts import render, redirect
from chat.forms import ConnectForm, CreateForm, RegisterForm, LoginForm, MessageForm, ChatMemberForm
from django.views import View
from django.contrib.auth import login, authenticate
from django.contrib import messages
from chat.models import Chat, Message, ChatMember, User
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse


class RegisterView(View):
    def get(self, request):
        return render(
            request=request,
            template_name='chat/register.html',
            context={
                'form': RegisterForm,
                'user': request.user
            }
        )

    def post(self, request):
        print('in register form')
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return redirect(to='/')
        else:
            return render(request, 'chat/register.html', {'form': form})


class LoginView(View):
    def get(self, request):
        return render(
            request=request,
            template_name='chat/login.html',
            context={
                'form': LoginForm
            }
        )

    def post(self, request):
        form = LoginForm(request, request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect('/')
            else:
                messages.error(request, "Invalid username or password.")
                return redirect('/login/')

        else:
            messages.error(request, "Invalid username or password.")
            return render(request, 'chat/login.html', {'form': form})


class ConnectView(LoginRequiredMixin, View):
    def get(self, request):
        return render(
            request=request,
            template_name='chat/connect.html',
            context={
                'form': ConnectForm
            }
        )

    def post(self, request):
        form = ConnectForm(request.POST)
        if form.is_valid():
            chat_id = form.cleaned_data['chat_id']
            return redirect(
                to=reverse('chat', kwargs={'chat_id': chat_id}))
        else:
            return render(request, 'chat/chat.html', {'form': form})


class CreateView(LoginRequiredMixin, View):
    def get(self, request):
        return render(
            request=request,
            template_name='chat/create.html',
            context={
                'form': CreateForm
            }
        )

    def post(self, request):
        form = CreateForm(request.POST)
        if form.is_valid():
            chat = form.save(commit=False)
            if request.user.is_authenticated:
                chat.owner = request.user
                form.save(commit=True)
            # print(request.__dict__)

            return redirect(to=f'/chat/{chat.id}')


class ChatView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        chat_member = ChatMember.objects.filter(
            chat=self.kwargs.get('chat_id'),
            user=self.request.user)
        print(chat_member)
        return bool(chat_member)

    def get(self, request, chat_id):
        chat = Chat(id=chat_id)
        chat_messages = Message.objects.filter(chat=chat).order_by('-date_created')
        return render(
            request=request,
            template_name='chat/chat.html',
            context={
                'form': MessageForm(),
                'chat_id': chat_id,
                'user': request.user,
                'chat_messages': chat_messages
            }
        )

    def post(self, request, chat_id):
        form = MessageForm(request.POST)
        if form.is_valid():
            chat_messages = form.save(commit=False)
            chat_messages.author = request.user
            chat_messages.chat = Chat.objects.get(pk=chat_id)
            form.save(commit=True)
            return redirect(to=reverse('chat', kwargs={'chat_id': chat_id}))


class ChatMemberView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return True

    def get(self, request, chat_id):
        chat_members = User.objects.filter(
            pk__in=ChatMember.objects.filter(chat=chat_id).values_list('user'))

        return render(
            request=request,
            template_name='chat/add_member.html',
            context={
                'form': ChatMemberForm(),
                'chat_id': chat_id,
                'user': request.user,
                'chat_members': chat_members
            }
        )

    def post(self, request, chat_id):
        print('here')
        form = ChatMemberForm(request.POST)
        to_return = redirect(to=reverse('add_member', kwargs={'chat_id': chat_id}))
        # print(form.cleaned_data)
        if not form.is_valid():
            messages.error(request, f'Check username spelling')
            return to_return

        try:
            user_to_add = User.objects.get_by_natural_key(form.cleaned_data["user"])
        except User.DoesNotExist:
            messages.error(request, f'No user with such name')
            return to_return

        current_members = User.objects.filter(
            pk__in=ChatMember.objects.filter(chat=chat_id).values_list('user'))
        if user_to_add in current_members:
            messages.info(request, f'User {user_to_add} is in this chat already')
            return to_return

        chat_member = ChatMember(user=user_to_add, chat=Chat.objects.get(pk=chat_id))
        chat_member.save()
        messages.info(request, f'User {user_to_add} added successfully')
        return to_return

