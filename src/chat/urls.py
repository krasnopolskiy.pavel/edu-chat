from django.urls import path
from .views import (
    ConnectView, CreateView, ChatView, RegisterView, LoginView, ChatMemberView)
from django.views.generic.base import RedirectView

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('', RedirectView.as_view(url='connect')),
    path('connect/', ConnectView.as_view(), name='connect'),
    path('create/', CreateView.as_view(), name='create'),
    path('chat/<int:chat_id>', ChatView.as_view(), name='chat'),
    path('chat/addmember/<int:chat_id>', ChatMemberView.as_view(), name='add_member')
]