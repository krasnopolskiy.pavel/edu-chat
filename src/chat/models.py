from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager


class User(AbstractUser):
    display_name = models.CharField(max_length=128, null=False)
    date_updated = models.DateTimeField(auto_now=True)

    objects = UserManager()


class Chat(models.Model):
    name = models.CharField(blank=False, max_length=128, default='unnamed_chat')
    owner = models.ForeignKey(to=User, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)


class ChatMember(models.Model):
    chat = models.ForeignKey(to=Chat, on_delete=models.CASCADE)
    user = models.ForeignKey(
        to=User, on_delete=models.CASCADE,
        help_text='user, allowed to read and write into this chat')


class Message(models.Model):
    chat = models.ForeignKey(to=Chat, on_delete=models.CASCADE)
    author = models.ForeignKey(to=User, on_delete=models.CASCADE)
    text = models.CharField(max_length=2000)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
