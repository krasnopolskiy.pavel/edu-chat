

#!/bin/bash
base_python_interpreter="/home/ubuntu/django_venv/bin/python"
project_domain="54.165.69.133"
project_path=`pwd`



# sudo ln -s $project_path/nginx/site.conf /etc/nginx/sites-enabled/
# sudo ln -s $project_path/systemd/gunicorn.service /etc/systemd/system/
# sudo ln -s $project_path/systemd/gunicorn.socket /etc/systemd/system/

sudo systemctl stop gunicorn.service
sudo systemctl restart gunicorn.socket
echo 'stoped gunicorn'
rm -rf ~/edu-chat/static/
echo 'removed old static'
export $(grep -v '^#' ~/edu-chat/secrets | xargs)
echo 'Added env variables'
`$base_python_interpreter ~/edu-chat/src/manage.py collectstatic`
echo 'collectstatic done'
`$base_python_interpreter ~/edu-chat/src/manage.py makemigrations`
echo 'makemigrations done'
`$base_python_interpreter ~/edu-chat/src/manage.py migrate`
echo 'migrate done'
sudo systemctl daemon-reload
echo 'daemon-reload done'
sudo systemctl restart gunicorn.socket
echo 'restart gunicorn.socket done'
sudo systemctl enable gunicorn.socket
echo 'enable gunicorn.socket done'
sudo systemctl restart gunicorn.service
echo 'restart gunicorn.service done'
sudo systemctl enable gunicorn.service
echo 'enable gunicorn.service done'
sudo service nginx restart
echo 'nginx restart'
