# Chat Application
Just an edu project with Django and PostgreSQL<br><br>
Current version uses nginx as proxy and gunicorn as wsgi server and running on AWS EC2 instance.<br>
You can have a try at [link](http://54.165.69.133/register/)<br>


## Update log:
### 09.01.2023
##### Updates:
- basic authorization (now u need to be logined to see/create chat)
- advanced authorization (u can see certain chat only if owner add you to this chat as a chat member)
- an "add member" page
- styling upgrades (at least it looks acceptable for testing BE application)
- some minor improvements
#### Next steps:
- invitation to chat 
- removing from chat
- refactoring and audit for all views

### 28.12.2022
Basic functionality. You can: create, connect to chat, register/login in account. No authorization provided yet (anyone can read your chat)
#### Next steps:
- Some styling upgrades (css for current pages, message author display, display current user)
- Basic authorization (storing chat members and granting access to them only)
- Invitation to chat logic
